from logging import debug
import logging
import unittest
from numpy.random import randint
from insertion_sort import insertion_sort


class TestInsertionSort(unittest.TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)

    def check_instance(self, input_, output):
        self.assertEqual(len(input_), len(output))
        for i in range(len(output)-1):
            self.assertLessEqual(output[i], output[i+1])

    def test_insertion_sort(self):
        for i in range(100):
            input_ = randint(low=-100, high=100, size=1000)
            result = list(input_)
            insertion_sort(result)
            self.check_instance(input_, result)
            debug('{} of {} instances tested'.format(i, 100))

if __name__ == '__main__':
    unittest.main()