\documentclass{beamer}
%\documentclass[handout]{beamer}
%\usetheme{Singapore}
\usetheme{Warsaw}

%\setbeamertemplate{footline}[frame number]
\newcommand*\oldmacro{}%
\let\oldmacro\insertshorttitle%
\renewcommand*\insertshorttitle{%
	\oldmacro\hfill%
	\insertframenumber\,/\,\inserttotalframenumber}


\setbeamercovered{transparent}

\usepackage[procnames]{listings}
\usepackage{color}
\usepackage{clrscode}

\title{Growth of Functions (CLRS 3)}
\subtitle{SER501 - Lecture Notes}
\author{John C. Femiani}




\begin{document}
    
\definecolor{keywords}{RGB}{255,0,90}
\definecolor{comments}{RGB}{0,0,113}
\definecolor{red}{RGB}{160,0,0}
\definecolor{green}{RGB}{0,150,0}
\lstset{language=Python, 
    basicstyle=\ttfamily\small, 
    keywordstyle=\color{keywords},
    commentstyle=\color{comments},
    stringstyle=\color{red},
    showstringspaces=false,
    identifierstyle=\color{green},
    procnamekeys={def,class},
    numbers=left}
   
\begin{frame}
    \titlepage
    
\end{frame}

\begin{frame} 
    \frametitle{Outline}
    \tableofcontents
\end{frame}


\section{Asymptotic notation}


\begin{frame} {Overview}
	\begin{itemize}
		\item<+-> A way to describe the behavior of functions \emph{in the limit}. We're studying \emph{asymptotic} efficiency.
		\item<+-> Describe the \emph{growth} of functions. 
		\item<+-> Focus on what's important by abstracting away lower-order terms and constant factors. 
		\item<+-> How we indicate running times of algorithms. 
		\item<+-> A way to compare sizes of functions:
		\begin{align*}
		 O      & \approx \leq \\
	     \Omega & \approx \geq \\
		 \Theta & \approx =    \\
		 o      & \approx <    \\ 
		 \omega & \approx >    \\
		\end{align*}
	\end{itemize}
\end{frame}

\subsection{Big $O$, $\Omega$, and $\Theta$}


\begin{frame}
	\frametitle{Example of $O$}
	
	\begin{definition}[$O$]	
		$$O(g(n)) = \{f(n) : \exists c>0, n_0, \forall n \geq n_0,  0 \leq f(n) \leq c g(n) \} $$
	\end{definition}

	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth, trim={40cm 3cm 40cm 0}, clip]{"IM/Chapter 3/Fig-3-1"}
			\end{figure}
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{block}{Hint}
				Asymptotic \emph{upper} bound, $\approx f \leq g$
			\end{block}
			
			\begin{example}
				$2 n^2 = O(n^3)$, with $c=1$ and $n_0=2$.
			\end{example}
			\pause
			\begin{alertblock}{Important}
				\alert{You must specify $c$ and $n_0$ to prove by the definitions!}		
			\end{alertblock}
		\end{column}
	\end{columns}

	

\end{frame}

\begin{frame}
	\frametitle{Example of $O$}
	
	\begin{definition}[$O$]	
		$O(g(n)) = \{f(n) : \exists c>0, n_0, \forall n \geq n_0,  0 \leq f(n) \leq c g(n) \} $
	\end{definition}

	\begin{example}[\ functions in $O(n^2)$\ ]
		\begin{itemize}[<+->]
			\item $n^2$  \hfill $ \leq 1 n^2 , \ n \geq 0$
			\item $n^2 + n$  \hfill  $ \leq 2 n^2 ,\  n \geq 1$
			\item $n^2 + 1000n$  \hfill  $ \leq 2 n^2 ,\  n \geq 1000$
			\item $1000n^2 + 1000n$  \hfill  $ \leq 1001 n^2 ,\  n \geq 1000$	
		\end{itemize}
		\pause Also
		\begin{itemize}[<+->]
			\item $n$  \hfill $ \leq 1 n^2 , \ n \geq 1$
			\item $n/1000$  \hfill $ \leq 1 n^2 , \ n \geq 0.001$
			\item $n^{1.9}$  \hfill $ \leq 1 n^2 , \ n \geq 1$
		\end{itemize}	
	\end{example}

\end{frame}

\begin{frame}
	\frametitle{But why don't constant factors matter?}
	
	\begin{enumerate}[<+->]
		\item Constant factors \emph{do} matter but not nearly as much as higher order terms.
		\item When two functions have the same complexity (like \textsc{Merge-Sort} and \textsc{Heap-Sort}) we consider constants, space usage, etc. 
		\item We are much \emph{more} concerned about when the ration of $f(n)$ to $g(n)$ is \emph{unbounded} as $n$ gets larger.
	\end{enumerate}

	
\end{frame}

\begin{frame}
	\frametitle{Definition of $\Omega$}

	\begin{definition}[$\Omega$]
		
		$$\Omega(g(n)) = \{f(n) : \exists c, n_0, \forall n \geq n_0,  0 \leq c g(n) \leq f(n) \} $$	
	\end{definition}

	\begin{columns}
		\begin{column}{0.4\textwidth}
		        \begin{figure}
				\includegraphics[width=1.0\textwidth, trim={80cm 3cm 0cm 0}, clip]{"IM/Chapter 3/Fig-3-1"}
				\end{figure}
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{block}{Hint}
				Assymptotic \emph{lower} bound, $\approx f \geq g$
			\end{block}
			\begin{example}
				$\sqrt{n} = \Omega(\lg n), \text{with $c=1$ and $n_0=16$} $
			\end{example}
			\pause
			\begin{alertblock}{Important}
				\alert{You must (still) specify $c$ and $n_0$ to prove by the definitions!}
			\end{alertblock}
		\end{column}
	\end{columns}
	

\end{frame}

\begin{frame}
	\frametitle{Examples of $\Omega$}
	
	\begin{definition}[$\Omega$]
		
		$$\Omega(g(n)) = \{f(n) : \exists c, n_0, \forall n \geq n_0,  0 \leq c g(n) \leq f(n) \} $$
	\end{definition}

	\begin{example}[\ functions in $\Omega(n^2)$ \ ]
	\begin{itemize}[<+->]
		\item $n^2$  \hfill $ \geq 1 n^2 , \ n \geq 0$
		\item $n^2 + n$  \hfill  $ \geq n^2 ,\  n \geq 1$
		\item $n^2 - n$  \hfill  $ \geq \frac{1}{2} n^2 ,\  n \geq 2$	
		\item $1000n^2 - 1000n$  \hfill  $ \geq 500 n^2 ,\  n \geq 2$	
	\end{itemize}
	\pause
	Also $n^3, n^{2.001}, n^2 \lg n, 2^n, ...$
	\end{example}
\end{frame}


\begin{frame} {$\Theta$ notation}
	
	\begin{definition}[$\Theta$]
		
		$\Theta(g) = \left\{f(n) : \exists c_1, c_2, n_0, \forall n \geq n_0,  0 \leq c_1 g(n) \leq f(n) \leq c_2 g(n) \right\} $
	\end{definition}
	
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\includegraphics[width=1.0\textwidth, trim={0 6.4cm 80cm 0}, clip]{"IM/Chapter 3/Fig-3-1"}
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{block}<3>{Hint}
				Instead of trying to prove $f =\Theta(g)$ using the definition, consider trying to prove $f=O(g)$ and $f=\Omega(g)$
			\end{block}
		\end{column}
	\end{columns}
	
	\pause
	\begin{theorem}{}
		$$f(n) = \Theta(g(n)) \iff f=O(g(n)) \wedge f=\Omega(g(n))$$
	\end{theorem}	
	
\end{frame}

\begin{frame}
	\frametitle{Working with assymptotic notation}
	
	When on the right hand side of an equation
	\begin{itemize}
		\item $O(n^2)$ is an anonymous function in the set $O(n^2)$
		\item Each time we see $O(g)$  it is a \emph{new} anonymous function with new constants etc.\
		\item On the RHS, \emph{there exists} some function that satisfies the equation. 
	\end{itemize} 
	
	When on the left hand side of an equations
	\begin{itemize}
		\item $O(g)$ means the equation is true \emph{for all} function in the set.
	\end{itemize}
	\begin{exampleblock}{Example}
		We interpret $ 2n^2 + \Theta(n) = \Theta(n^2)$ as \emph{for all} functions $f(n)\in\Theta(n)$, \emph{there exists} a function $g(n) \in \Theta(n^2)$ such that $2n^2 + f(n) = g(n)$.
	\end{exampleblock}
\end{frame}

\subsection{little $o$ and little $\omega$}

\begin{frame}
	\frametitle{Definition of little-$o$}
	
	\begin{definition}[little $o$]
		
		$o(g(n)) = \{f(n) : \forall c>0, \exists n_0, \forall n \geq n_0,  0 \leq f(n) < c g(n) \} $	
	\end{definition}

	\begin{block}{Hint 1}
	Asymptotic \emph{strict upper} bount, $\approx f < g$.
	\end{block}	
	
	\begin{alertblock}{Hint 2 - Very useful!}
	   $$ \lim_{n\rightarrow\infty} \frac{f(n)}{g(n)} = 0 $$
	\end{alertblock}	
	
\end{frame}

\begin{frame}
	\frametitle{Examples of little-$o$}
	
	\begin{definition}[little $o$]
		
		$o(g(n)) = \{f(n) : \forall c>0, \exists n_0, \forall n \geq n_0,  0 \leq f(n) < c g(n) \} $	
	\end{definition}
	
	\begin{exampleblock}{Examples}
	\begin{itemize}[<+->]
		\item $n^{1.999} = o(n^2)$ \hfill $\lim n^{-0.001} = 0$
		\item $n^2/\lg n = o(n^2)$ \hfill $\lim 1/\lg n = 0$
		\item $n^2 \neq o(n^2)$ \hfill $c=1$, just like $2 \not < 2$
	\end{itemize}	
	\end{exampleblock}	
\end{frame}



\begin{frame}
	
		\frametitle{Definition of little-$\omega$}
		
		\begin{definition}[little $\omega$]
			\centering
			$\omega(g(n)) = \{f(n) : \forall c>0, \exists n_0, \forall n \geq n_0,  0 \leq c g(n) < f(n) \} $	
		\end{definition}
		
		\begin{block}{Hint 1}
			Asymptotic \emph{strict lower} bound, $\approx f > g$.
		\end{block}	
		
		\begin{alertblock}{Hint 2 - Very useful!}
			$$ \lim_{n\rightarrow\infty} \frac{f(n)}{g(n)} = \infty $$
		\end{alertblock}	
		
\end{frame}

\begin{frame}
	\frametitle{Examples of little-$\omega$}
	
	\begin{definition}[little $\omega$]
		\centering
		$\omega(g(n)) = \{f(n) : \forall c>0, \exists n_0, \forall n \geq n_0,  0 \leq c g(n) < f(n) \} $
	\end{definition}
	
	\begin{exampleblock}{Examples}
		\begin{itemize}[<+->]
			\item $n^{2.001} = \omega(n^2)$ \hfill $\lim n^{0.001} = \infty$
			\item $n^2 \lg n = \omega(n^2)$ \hfill $\lim \lg n = \infty$
			\item $n^2 \neq \omega(n^2)$ \hfill $c=1$, just like $2 \not > 2$
		\end{itemize}	
	\end{exampleblock}	
\end{frame}

\subsection{Useful Properties}

\begin{frame}[fragile]
	\frametitle{Advice for problem sets} 

	You can used the \texttt{sympy} module to calculate limits, and quickly answer problems in many cases.  This has the added benefit of getting you more python practice. Use the ipython QtConsole or ipython notebook interface. 
	
	\begin{block}{}
	\begin{lstlisting}
from sympy import *
n = sympy.symbols('n', positive=true)
lg = lambda n: log(n, 2)
def little_o(f, g): 
	return limit(f/g, n, oo) == 0
	\end{lstlisting}
	\end{block}
	
	\begin{alertblock}{Watch out for undefined}
		Some limits are undefined, watch out and double check sympy (or any tool's) results.
	\end{alertblock}
\end{frame}


\begin{frame}
	 \frametitle{Useful propertys (helpful for homework)}
	 \begin{description}[<+->]
	 	\item[Transitivity]
	 	$f = \Theta(g),  g=\Theta(h) \implies f=\Theta(h)$. \\
	 	Same for $O, \Omega, o,$ and $\omega$.
	 	
	 	\item[Reflexivity]
	 	$f(n) = \Theta(f(n))$ \\
	 	also for $O, \Omega$. What about $o$ and $\omega$?
	 
		\item[Symmetry]
		$f = \Theta(g) \iff g = \Theta(f)$
		
		\item[Transpose Symmetry]  \ \\
		$f = O(g) \iff g = \Omega(f)$.\\
		$f = o(g) \iff g = \omega(f)$.
		
	 \end{description}
	 \pause
	 If $f=o(g)$, can $f=\Omega(g)$? $\omega(g)?$ $\Theta(g)$?\\
	 \pause What if $f=\Theta(g)$? What can we say about $O, \Omega, o, \omega$?
\end{frame}

\begin{frame}
	\frametitle{Comparisons}
	We use terms for functions that are similar to terms we use for real numbers
	\begin{itemize}
		\item $f$ is \emph{asymptotically smaller} than $g$ if $f=o(g)$
		\item $f$ is \emph{asymptotically larger} than $g$ if $f=\omega(g)$.
	\end{itemize}
		
	There is \emph{no trichotomy} 
	\begin{itemize}
		\item $f$ does \emph{not} have to be in one of $o, \Theta, \omega$.
		\item This is unlike real numbers, where one of $a<b, a=b,$ or $a>b$ must be true. 
	\end{itemize} 
	\begin{exampleblock}{Example}
		$n^{1+\sin n}$ is not $o(n), O(n), \Theta(n), \Omega(n),$ or $\omega(n)$, since $1+sin(n)$ osculates between 0 and 2. 
	\end{exampleblock}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	
	There are some useful properties of limits, which can be easier to use than the formal definitions as long as they apply.
	 
	If $\lim\limits_{n\rightarrow\infty} f/g $ is defined...
		$$f = \begin{cases}
		 o(g) & \text{if } \lim\limits_{n\rightarrow\infty} f/g = 0\\
		\omega(g) & \text{if } \lim\limits_{n\rightarrow\infty} f/g = \infty\\
		\Theta(g) & \text{else (if defined)} \\
		\end{cases}$$
	Also
		$$f = \begin{cases}
		O(g) & \text{if } \lim\limits_{n\rightarrow\infty} f/g < \infty \\
		\Omega(g) & \text{if } \lim\limits_{n\rightarrow\infty} f/g > 0\\
		\end{cases}$$
	If the limit is undefined, you have to be clever.  Limits can \emph{often} be calculated easily by canceling out terms in $f/g$,  or by using symbolic math packages. 
	
\end{frame}

\section{Standard notations and common function}

\subsection{Functions}

\begin{frame}
	\frametitle{Standard Notation and Common Functions}
	\framesubtitle{Monoticity}
	\begin{itemize}[<+->]
		\item $f(n)$ is \alert{monotonically increasing} if $m \leq n \implies f(m) \leq f(n)$.
		\item $f(n)$ is \alert{monotonically decreasing} if $m \leq n \implies f(m) \geq f(n)$.
		\item $f(n)$ is \alert{strictly increasing} if $m < n \implies f(m) < f(n)$.
		\item $f(n)$ is \alert{strictly decreasing} if $m < n \implies f(m) > f(n)$.
		
	\end{itemize}
	
\end{frame}


\subsection{Logarithms}

\begin{frame}
	\frametitle{Logarithms}
	\framesubtitle{Notation}
	
	\begin{definition}[logarithm]
		The \alert{logarithm} of $n$ tells how many times a base number $b$ must be multiplied by itself to produce $n$, 	$\log_b a = k  \iff b^k = a$.
	\end{definition} 
	\vspace{-0.5cm}
	\begin{align*}
	\lg n &= \log_2 n & \text{(binary logarithm)}\\
	\ln n &= \log_e n & \text{(natural logarithm)} \\
	\lg^k n &= (\lg n)^k & \text{(exponentiation)}\\
	\lg \lg n &= \lg ( \lg n) & \text{(composition)} \\
	\lg n + k &= (\lg n) + k & \text{(not $\lg (n+k)$)}
	\end{align*}
	
	In the expression $\log_b a$:
	\begin{itemize}
		\item it is strictly increasing for $a$, with $b$ held constant.
		\item it is strictly decreasing for $b$, with $a$ held constant. 
	\end{itemize}
	\vspace{1cm}
\end{frame}

\begin{frame}
	\frametitle{Logarithms}
	\framesubtitle{Logarithmic Identities}
	For all real $a>0, b>0, c>0$, and $n$ where bases are not $1$: 
	\begin{align*}
	\uncover<+>{ a &= b^{\log_b a},\\}
	\uncover<+>{\log_c(ab) &= \log_c a + \log_c b,\\}
	\uncover<+>{\log_b a^n &= n \log_b a, \\}
	\uncover<+>{\log_b a &= \frac{\log_c a}{\log_c b}, & \text{(3.15)} \\}
	\uncover<+>{\log_b (1/a) &= -\log_b a, \\}
	\uncover<+>{\log_b a &= \frac{1}{\log_a b}, \\}
	\uncover<+>{a^{\log_b c} &= c^{\log_b a}& \text{(3.16)}.}	
	\end{align*}
\end{frame}

\begin{frame}
	\frametitle{Logarithms}
	\framesubtitle{Useful properties}
	
	\begin{itemize}[<+->]
		\item Changing the base of a logarithm from one constant to  another only changes the value by a \alert{constant factor} \\
		(so the base does not usually matter in asymptotic notation)
		
		\item Convention is to use $\lg$ (the base 2) unless the base actually matters.
		
		\item  Logarithms grow more slowly than polynomials
	\end{itemize}
\end{frame}

\subsection{Factorials}

\begin{frame}
	\frametitle{Factorials}
	\begin{definition}[Factorial]
		$$n! = 1\cdot 2 \cdot 3 \cdots n, \ \ \  0! =1.$$
	\end{definition}
	\pause
	We can use \alert{Stirling's approximation} in order to relate factorials to exponentials.
	
	$$
	n! = \sqrt{2 \pi n}\left(\frac{n}{e}\right)^n \left(1 + \Theta\left(\frac{1}{n}\right)\right).
	$$
	\pause
	One result you might find useful:
	$$ lg(n!) = \Theta(n \lg n).$$
\end{frame}
\end{document}
