def insertion_sort(A):
    """
    :param A: A list of numbers <x_1, x_2, ..., x_n>
    :type A: list[int]
    :return: A permutation of input that is in order.

    >>> insertion_sort([5, 2, 3, 2, 4, 1])
    [1, 2, 2, 3, 4, 5]

    """
    n = len(A)
    for j in range(1, n):
        key = A[j]
        i = j - 1
        while i >= 0 and A[i] > key:
            A[i + 1] = A[i]
            i -= 1
        A[i + 1] = key
    return A
